const {LoadModel} = require('../models/load');
const {TruckModel} = require('../models/truck');

const getTruckType = (load) => {
  const {dimensions, payload} = load;
  if (dimensions.length < 300 &&
    dimensions.height < 250 &&
    dimensions.width < 170 &&
    payload < 1700) {
    return 'SPRINTER';
  }
  if (dimensions.length < 500 &&
    dimensions.height < 250 &&
    dimensions.width < 170 &&
    payload < 2500) {
    return 'SMALL STRAIGHT';
  }
  if (dimensions.length < 700 &&
    dimensions.height < 350 &&
    dimensions.width < 200 &&
    payload < 4000) {
    return 'LARGE STRAIGHT';
  }
  return null;
};

const findDriver = async (id) => {
  const load = await LoadModel.findOne({_id: id});
  const type = getTruckType(load);
  if (!type) {
    return null;
  }
  try {
    const truck = await TruckModel.findOne({
      status: 'IS', type,
      assigned_to: {'$nin': [undefined, null]},
    });
    truck.status = 'OL';
    await truck.save();
    return truck;
  } catch {
    return null;
  }
};

module.exports.findDriver = findDriver;
