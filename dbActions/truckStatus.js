const {TruckModel} = require('../models/truck');

const updateTruckStatus = async (user, status) => {
  await TruckModel.findOneAndUpdate(
      {'assigned_to': user},
      {status},
      {
        runValidators: true,
        context: 'query',
      });
};

module.exports.updateTruckStatus = updateTruckStatus;
