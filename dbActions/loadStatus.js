const {LoadModel} = require('../models/load');

const updateLoadStatus = async (id, updates) => {
  await LoadModel.findOneAndUpdate(
      {_id: id},
      updates,
      {
        runValidators: true,
        context: 'query',
      });
};

const updateLoadStatusForDriver = async (user, updates) => {
  try {
    await LoadModel.findOneAndUpdate(
        {
          assigned_to: user,
          status: 'ASSIGNED',
        },
        updates,
        {
          runValidators: true,
          context: 'query',
        });
    return true;
  } catch {
    return false;
  }
};

module.exports.updateLoadStatus = updateLoadStatus;
module.exports.updateLoadStatusForDriver = updateLoadStatusForDriver;
