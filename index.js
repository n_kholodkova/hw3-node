const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
const fs = require('fs');
const cors = require('cors');
require('dotenv').config();
const {dataBase} = require('./config');
const {authRouter} = require('./routes/authRoutes');
const {meRouter} = require('./routes/meRoutes');
const {trucksRouter} = require('./routes/trucksRoutes');
const {loadsRouter} = require('./routes/loadsRoutes');

const PORT = process.env.PORT || 8080;

const expressApp = express();
expressApp.use(cors());
expressApp.use(express.json());
expressApp.use(morgan('common', {
  stream: fs.createWriteStream('./logs.log', {flags: 'a'}),
}));

expressApp.use(morgan('dev'));

try {
  mongoose.connect(dataBase.uri).then(() => expressApp.listen(PORT, () => {
    console.log(`App is running on port ${PORT}...`);
  }));
} catch (error) {
  console.log('Cannot connect to DB');
}

expressApp.use('/', authRouter);
expressApp.use('/', meRouter);
expressApp.use('/', trucksRouter);
expressApp.use('/', loadsRouter);
