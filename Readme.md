# Very simple server with Node.js and Express.js. Used JWT Authentication.

### Routes:
- #### GET

  - /api/users/me

    return user's data. User can request only his own profile info. Requires JWT token in headers.

  - /api/trucks

    return the list of trucks for authorized user(abailable only for driver role). Requires JWT token in headers.

  - /api/trucks/:truckid

    return user's truck by id(abailable only for driver role). Requires JWT token in headers.

  - /api/loads/

    return the list of loads for authorized user, returns list of completed and active loads for Driver and list of all available loads for Shipper. Has optional parameters in query:
      - status (filter by load status)
      - limit (limit for records per request, pagination parameter(default: 10, max: 50))
      - offset (offset for records, pagination parameter(default: 0))
    Requires JWT token in headers.

  - /api/loads/active

    return the active load for authorized driver(available only for driver). Requires JWT token in headers.

  - /api/loads/:loadid

    return user's Load by id. Requires JWT token in headers.


- #### POST

  - /api/auth/register

    register a new system user(Shipper or Driver). In body of request gets parameters as json in next format {email: "EMAIL", password: "PASSWORD", "role": "ROLE"}. Return error and status code 400 if user with given username already exists, otherwise success message and code 200.

  - /api/auth/login

    login into the system. In body of request gets parameters as json in next format {email: "EMAIL", password: "PASSWORD"}. Return error and status code 400 if user with given username already exists, otherwise success with JWT token message and code 200.

  - /api/trucks

    add Truck for User (abailable only for driver role). In body of request gets parameters as json in next format {type: "TRUCK TYPE"}. Return error status code 400 if truck does not exist. Otherwise success message and code 200 and create new truck. Requires JWT token in headers.

  - /api/trucks/:truckid/assign

    assign truck to user by id (abailable only for driver role). Requires JWT token in headers.

  - /api/loads

    add Load for User(abailable only for shipper role). In body of request gets parameters as json in next format {name: "TITLE OF LOAD", payload: "WEIGHT", pickup_address: "ADDRESS", delivery_address: "ADDRESS", dimensions: {width: WIDTH, length: LENGTH, height: HEIGHT. Requires JWT token in headers.

  - /api/loads/:loadid

    post a user's load by id, search for drivers (abailable only for shipper role). Update load's status. Requires JWT token in headers.

- #### PUT

  - /api/trucks/:truckid

    update user's truck by id (abailable only for driver role). in body of request gets parameters as json in next format {type: "TRUCK TYPE"}. Return error status code 400 if truck does not exist. Otherwise success message and code 200 and update truck. Requires JWT token in headers.

  - /api/loads/:loadid

  update user's load by id (abailable only for shipper role). In body of request gets parameters as json in next format {name: "TITLE OF LOAD", payload: "WEIGHT", pickup_address: "ADDRESS", delivery_address: "ADDRESS", dimensions: {width: WIDTH, length: LENGTH, height: HEIGHT. Requires JWT token in headers.

- #### PATCH

  - /api/users/me

    in body of request gets parameters as json in next format {oldPassword: "OLDPASSWORD", newPassword: "NEWPASSWORD"}. Return error and status code 400 if user with given username does not exist or oldPassword is invalid, otherwise success message and code 200. Requires JWT token in headers.

  - /api/loads/active/state

    iterate to next Load state(available only for driver). Requires JWT token in headers.

- #### DELETE

  - /api/users/me

    Return success message and code 200 and deletes user. Requires JWT token in headers.

  - /api/trucks/:truckid

    delete user's truck by id. Return error status code 400 if truck does not exist, otherwise success message and code 200. Requires JWT token in headers.

  - /api/loads/:loadid

    delete user's load by id. Return error status code 400 if load does not exist, otherwise success message and code 200. Requires JWT token in headers.

To run use `npm install` and `npm start`
