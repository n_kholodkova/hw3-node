const {TruckModel} = require('../models/truck');
const {UserModel} = require('../models/user');

const isTruckAssignedToDriver = async (driverId, truckId) => {
  const truck = await TruckModel.findOne({_id: truckId});
  return driverId === truck.assigned_to?.toString() ?? '';
};

const getAssignedTrucksToDriver = async (driverId) => {
  const user = await UserModel.findOne({_id: driverId});
  const truck = await TruckModel.findOne({assigned_to: user});
  if (!truck) {
    return true;
  }
  if (truck.status !== 'IS') {
    return false;
  }
  truck.assigned_to = undefined;
  await truck.save();
  return true;
};

const getTruckModelToResponse = (truck) => {
  const {_id, created_by, assigned_to = '', type, status, createdAt} = truck;
  return {_id, created_by, assigned_to, type, status,
    created_date: new Date(createdAt),
  };
};

module.exports.isTruckAssignedToDriver = isTruckAssignedToDriver;
module.exports.getAssignedTrucksToDriver = getAssignedTrucksToDriver;
module.exports.getTruckModelToResponse = getTruckModelToResponse;
