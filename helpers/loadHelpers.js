const {
  loadStatusSchema,
  loadLimitSchema,
  loadOffsetSchema,
} = require('./../joiSchemas/schemas');

const {LoadModel} = require('./../models/load');

const STATES = [
  'En route to Pick Up',
  'Arrived to Pick Up',
  'En route to delivery',
  'Arrived to delivery',
];

const getloadStatus = (status) => {
  const {error, value} = loadStatusSchema.validate(status);
  if (error) {
    return {error: 'Invalid status provided', value: null};
  }
  return {error: null, value};
};

const getloadLimit = (status) => {
  const {error, value} = loadLimitSchema.validate(status);
  if (error) {
    return {error: 'Limit must be positive and max 50', value: null};
  }
  return {error: null, value: value ?? 10};
};

const loadOffset = (status) => {
  const {error, value} = loadOffsetSchema.validate(status);
  if (error) {
    return {error: 'Offset must be positive', value: null};
  }
  return {error: null, value: value ?? 0};
};

const isLoadCreatedByUser = async (userId, loadId) => {
  const load = await LoadModel.findOne({_id: loadId});
  return userId === load.created_by?.toString() ?? '';
};

const isLoadAssignedToUser = async (userId, loadId) => {
  const load = await LoadModel.findOne({_id: loadId});
  return userId === load.assigned_to?.toString() ?? '';
};


const getNextState = async (user) => {
  const load = await LoadModel.findOne({assigned_to: user, status: 'ASSIGNED'});
  const currentState = load.state;
  return STATES[STATES.indexOf(currentState) + 1];
};

const getLoadModelToResponse = (load) => {
  const {_id, created_by, assigned_to, status,
    state, name, payload, pickup_address,
    delivery_address, dimensions, logs, createdAt} = load;
  return {
    _id, created_by, assigned_to, status,
    state, name, payload, pickup_address, delivery_address,
    dimensions, logs,
    created_date: new Date(createdAt),
  };
};

module.exports.getloadStatus = getloadStatus;
module.exports.getloadLimit = getloadLimit;
module.exports.getloadOffset = loadOffset;
module.exports.isLoadCreatedByUser = isLoadCreatedByUser;
module.exports.getLoadModelToResponse = getLoadModelToResponse;
module.exports.getNextState = getNextState;
module.exports.isLoadAssignedToUser = isLoadAssignedToUser;

