const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const LoadSchema = new Schema(
    {
      created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserModel',
        required: true,
      },
      logs: [
        {
          message: {
            type: String,
            required: [true, 'No message'],
          },
          time: {
            type: String,
            required: [true, 'No time'],
          },
        },
      ],
      assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserModel',
        default: null,
      },
      status: {
        type: String,
        default: 'NEW',
        required: true,
      },
      name: {
        type: String,
        required: [true, 'No name'],
      },
      state: {
        type: String,
        default: '',
      },
      pickup_address: {
        type: String,
        required: true,
      },
      delivery_address: {
        type: String,
        required: true,
      },
      dimensions: {
        width: {
          type: Number,
          required: [true, 'No width'],
        },
        height: {
          type: Number,
          required: [true, 'No height'],
        },
        length: {
          type: Number,
          required: [true, 'No lendth'],
        },
      },
      payload: {
        type: Number,
        required: true,
      },
    },
    {timestamps: true},
);

const LoadModel = mongoose.model('Load', LoadSchema);

module.exports.LoadModel = LoadModel;

