const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const bcryptjs = require('bcryptjs');

const UserSchema = new Schema(
    {
      role: {
        type: String,
        required: [true, 'Role is missing'],
      },
      password: {
        type: String,
        required: [true, 'Password is missing'],
      },
      email: {
        type: String,
        required: [true, 'Email is missing'],
        unique: [true, 'Email already in use'],
      },
    },
    {timestamps: true},
);

UserSchema.pre('save', async function(next) {
  if (!this.isModified('password')) {
    return next();
  }
  const salt = await bcryptjs.genSalt();
  const hashedPassword = await bcryptjs.hash(this.password, salt);
  this.password = hashedPassword;
  next();
});

UserSchema.pre('findOneAndUpdate', async function(next) {
  if (this._update.password) {
    const salt = await bcryptjs.genSalt();
    const hashed = await bcryptjs.hash(this._update.password, salt);
    this._update.password = hashed;
  }
  next();
});

const UserModel = mongoose.model('User', UserSchema);

module.exports.UserModel = UserModel;

