const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TruckSchema = new Schema(
    {
      created_by: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserModel',
        required: true,
      },
      assigned_to: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserModel',
        default: null,
      },
      status: {
        type: String,
        default: 'IS',
      },
      type: {
        type: String,
        required: [true, 'No type'],
      },
    },
    {timestamps: true},
);

const TruckModel = mongoose.model('Truck', TruckSchema);

module.exports.TruckModel = TruckModel;

