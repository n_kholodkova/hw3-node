const {LoadModel} = require('../models/load');
const {TruckModel} = require('../models/truck');
const {
  isLoadCreatedByUser,
  getLoadModelToResponse,
  isLoadAssignedToUser,
} = require('./../helpers/loadHelpers');
const {getTruckModelToResponse} = require('./../helpers/truckHelpers');
const {findDriver} = require('./../dbActions/loadToDriver');
const {updateLoadStatus} = require('./../dbActions/loadStatus');

const getLoad = async (request, response) => {
  const {id} = request.params;
  try {
    const load = await LoadModel.findById(id);
    if (!isLoadAssignedToUser(request.userId, id) ||
      !isLoadCreatedByUser(request.userId, id)) {
      response
          .status(400)
          .json({
            message:
            `Load with id ${id} does not assigned or created be user`,
          })
          .end();
      return;
    }
    response
        .status(200)
        .json({
          load: getLoadModelToResponse(load),
        });
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const updateLoad = async (request, response) => {
  const {id} = request.params;
  const {
    name,
    payload,
    pickup_address,
    delivery_address,
    dimensions,
  } = request.body;
  try {
    if (!await isLoadCreatedByUser(request.userId, id)) {
      response
          .status(400)
          .json({
            message: `Load with id ${id} does not belongs to user`,
          })
          .end();
      return;
    }
    await LoadModel.findOneAndUpdate({_id: id}, {
      name, payload, pickup_address,
      delivery_address, dimensions,
    },
    {
      runValidators: true,
      context: 'query',
      new: true,
    });
    response.status(200).json({message: 'Load details changed successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const deleteLoad = async (request, response) => {
  const {id} = request.params;
  if (!await isLoadCreatedByUser(request.userId, id)) {
    response
        .status(400)
        .json({message: `Load with id ${id} does not belongs to user`})
        .end();
    return;
  }
  try {
    await LoadModel.remove({_id: id});
    response.status(200).json({message: 'Load deleted successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const postLoad = async (request, response) => {
  const {id} = request.params;
  try {
    if (!await isLoadCreatedByUser(request.userId, id)) {
      response
          .status(400)
          .json({message: `Load with id ${id} does not belongs to user`})
          .end();
      return;
    }
    await updateLoadStatus(id, {
      status: 'POSTED',
      $push: {
        logs: {
          message: 'Load Posted',
          time: new Date().toUTCString(),
        },
      },
    });
    const truck = await findDriver(id);
    if (!truck) {
      await updateLoadStatus(id, {
        status: 'NEW',
        $push: {
          logs: {
            message: 'Cannot find driver. Status New',
            time: new Date().toUTCString(),
          },
        },
      });
      response.status(400).json({message: 'Cannot find truck'}).end();
      return;
    }
    await updateLoadStatus(id, {
      status: 'ASSIGNED',
      state: 'En route to Pick Up',
      assigned_to: truck.assigned_to,
      $push: {
        logs: [{
          message: 'Load Assigned',
          time: new Date().toUTCString(),
        },
        {
          message: 'Status changed to "En route to Pick Up"',
          time: new Date().toUTCString(),
        }],
      },
    });
    response.status(200).json({
      'message': 'Load posted successfully',
      'driver_found': true,
    });
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const getShippingInfo = async (request, response) => {
  const {id} = request.params;
  try {
    const load = await LoadModel.findOne({_id: id});
    if (!await isLoadCreatedByUser(request.userId, id)) {
      response.status(400).json({
        message: `Load with id ${id} does not belongs to user`,
      }).end();
      return;
    }
    const truck = await TruckModel.findOne({'assigned_to': load.assigned_to});
    response.status(200).json({
      load: getLoadModelToResponse(load),
      truck: getTruckModelToResponse(truck),
    });
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};


module.exports.getLoad = getLoad;
module.exports.updateLoad = updateLoad;
module.exports.deleteLoad = deleteLoad;
module.exports.postLoad = postLoad;
module.exports.getShippingInfo = getShippingInfo;
