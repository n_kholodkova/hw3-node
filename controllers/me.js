const bcryptjs = require('bcryptjs');
const {UserModel} = require('../models/user');

const getMe = async (request, response) => {
  try {
    const currentUser = await UserModel.findOne({_id: request.userId});
    if (!currentUser) {
      response.status(400).json({message: 'Invalid credentials'}).end();
      return;
    }
    response
        .status(200)
        .json({
          user: {
            _id: currentUser._id,
            role: currentUser.role, email: currentUser.email,
            created_date: new Date(currentUser.createdAt),
          },
        });
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const updateMe = async (request, response) => {
  try {
    const {oldPassword, newPassword} = request.body;
    const currentUser = await UserModel.findOne(
        {_id: request.userId});
    if (!currentUser) {
      response.status(400).json({message: 'Invalid credentials'}).end();
      return;
    }
    const isValidPassword = await bcryptjs
        .compare(oldPassword, currentUser.password);
    if (!isValidPassword) {
      response.status(400).json({message: 'Invalid password'}).end();
      return;
    }
    await UserModel.findOneAndUpdate(
        {_id: request.userId},
        {password: newPassword},
        {
          runValidators: true,
          context: 'query',
        });
    response.status(200).json({message: 'Password changed successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  }
};

const deleteMe = async (request, response) => {
  try {
    await UserModel.remove({_id: request.userId});
    response.status(200).json({message: 'Profile deleted successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

module.exports.getMe = getMe;
module.exports.updateMe = updateMe;
module.exports.deleteMe = deleteMe;
