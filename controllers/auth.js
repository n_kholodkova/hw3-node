const jwt = require('jsonwebtoken');
const bcryptjs = require('bcryptjs');
require('dotenv').config();
const {UserModel} = require('../models/user');

const JWT_AGE = 24 * 60 * 60;
const createJWTToken = (id) => {
  return jwt.sign({id}, process.env.JWT_SECRET, {
    expiresIn: JWT_AGE,
  });
};

const signUp = async (request, response) => {
  const {email, password, role} = request.body;
  try {
    await UserModel.create({email, password, role});
    response.status(200).json({message: 'Profile created successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const signIn = async (request, response) => {
  try {
    const {email, password} = request.body;
    const currentUser = await UserModel.findOne({email});
    if (!currentUser) {
      response.status(400).json({message: 'Invalid email'}).end();
      return;
    }
    const isValidPassword = await bcryptjs
        .compare(password, currentUser.password);
    if (!isValidPassword) {
      response.status(400).json({message: 'Invalid password'}).end();
      return;
    }
    const JWTtoken = createJWTToken(currentUser._id);
    response.status(200).json({jwt_token: JWTtoken});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
    return;
  }
};

const updatePassword = async (request, response) => {
  try {
    const {email} = request.body;
    const currentUser = await UserModel.findOne({email});
    if (!currentUser) {
      response.status(400).json({message: 'Invalid email'}).end();
      return;
    }
    response.status(200)
        .json({message: 'New password sent to your email address'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
    return;
  }
};

module.exports.signUp = signUp;
module.exports.signIn = signIn;
module.exports.updatePassword = updatePassword;
