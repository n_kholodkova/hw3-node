const {TruckModel} = require('../models/truck');
const {UserModel} = require('../models/user');
const {
  isTruckAssignedToDriver,
  getAssignedTrucksToDriver,
  getTruckModelToResponse,
} = require('./../helpers/truckHelpers');

const getTrucks = async (request, response) => {
  try {
    const trucksList = await TruckModel.find() ?? [];
    const trucks = Array.from(trucksList)
        .map((truck) => getTruckModelToResponse(truck));
    response.status(200).json({trucks});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const createTruck = async (request, response) => {
  const {type} = request.body;
  try {
    const user = await UserModel.findById(request.userId);
    await TruckModel.create({type, created_by: user});
    response.status(200).json({message: 'Truck created successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const getTruck = async (request, response) => {
  const {id} = request.params;
  try {
    const truck = await TruckModel.findById(id);
    if (!truck) {
      response
          .status(200)
          .json({truck: {}});
    }
    response
        .status(200)
        .json({truck: getTruckModelToResponse(truck)});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const updateTruck = async (request, response) => {
  const {type} = request.body;
  const {id} = request.params;
  if (await isTruckAssignedToDriver(request.userId, id)) {
    response
        .status(400)
        .json({message: 'Cannot update asigned trucks'})
        .end();
    return;
  }
  try {
    await TruckModel.findOneAndUpdate(
        {_id: id},
        {type: type},
        {
          runValidators: true,
          context: 'query',
          new: true,
        });
    response.status(200).json({message: 'Truck details changed successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  }
};

const assignTruck = async (request, response) => {
  const {id} = request.params;
  if (!await getAssignedTrucksToDriver(request.userId)) {
    response
        .status(400)
        .json({message: 'Driver has already assigned truck'})
        .end();
    return;
  }
  try {
    const user = await UserModel.findById(request.userId);
    await TruckModel.findOneAndUpdate(
        {_id: id},
        {assigned_to: user},
        {
          runValidators: true,
          context: 'query',
        });
    response.status(200).json({message: 'Truck assigned successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  }
};

const deleteTruck = async (request, response) => {
  const {id} = request.params;
  if (await isTruckAssignedToDriver(request.userId, id)) {
    response
        .status(400)
        .json({message: 'Cannot delete asigned trucks'})
        .end();
    return;
  }
  try {
    await TruckModel.remove({_id: id});
    response.status(200).json({message: 'Truck deleted successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

module.exports.getTrucks = getTrucks;
module.exports.createTruck = createTruck;
module.exports.getTruck = getTruck;
module.exports.updateTruck = updateTruck;
module.exports.assignTruck = assignTruck;
module.exports.deleteTruck = deleteTruck;
