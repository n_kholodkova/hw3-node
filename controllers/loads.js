const {LoadModel} = require('../models/load');
const {UserModel} = require('../models/user');
const {
  getloadStatus,
  getloadLimit,
  getloadOffset,
  getLoadModelToResponse,
  getNextState,
} = require('./../helpers/loadHelpers');
const {updateTruckStatus} = require('./../dbActions/truckStatus');
const {updateLoadStatusForDriver} = require('./../dbActions/loadStatus');
const ObjId = require('mongoose').Types.ObjectId;

const getLoads = async (request, response) => {
  const {status, limit, offset} = request.query;
  const {error: statusError, value: statusValue} = getloadStatus(status);
  if (statusError) {
    return response.status(400).json({message: statusError}).end();
  }
  const {error: limitError, value: limitValue} = getloadLimit(limit);
  if (limitError) {
    return response.status(400).json({message: limitError}).end();
  }
  const {error: offsetError, value: offsetValue} = getloadOffset(offset);
  if (offsetError) {
    return response.status(400).json({message: offsetError}).end();
  }
  const user = await UserModel.findOne({_id: request.userId});
  const params = {};
  if (statusValue) {
    params['status'] = statusValue.toUpperCase();
  }
  if (user.role === 'DRIVER') {
    params['assigned_to'] = new ObjId(request.userId);
  } else {
    params['created_by'] = new ObjId(request.userId);
  }
  try {
    const loadList = await LoadModel
        .find(params)
        .skip(parseInt(offsetValue))
        .limit(parseInt(limitValue)) ?? [];
    const loads = Array.from(loadList)
        .map((load) => getLoadModelToResponse(load));
    response.status(200).json({loads});
  } catch (error) {
    response.status(400).json({error: error.message}).end();
  };
};

const createLoad = async (request, response) => {
  const {name, payload, pickup_address,
    delivery_address, dimensions} = request.body;
  try {
    const user = await UserModel.findById(request.userId);
    await LoadModel
        .create({
          name, payload, pickup_address,
          delivery_address, dimensions, created_by: user,
        });
    response.status(200).json({message: 'Load created successfully'});
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const getActiveLoad = async (request, response) => {
  try {
    const load = await LoadModel.findOne(
        {assigned_to: new ObjId(request.userId), status: 'ASSIGNED'});
    if (!load) {
      return response.status(200).json({load: {}}).end();
    }
    response.status(200).json({
      load: getLoadModelToResponse(load),
    });
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

const changeLoadState = async (request, response) => {
  try {
    const user = await UserModel.findById(request.userId);
    const nextState = await getNextState(user);
    if (nextState) {
      const update = {
        state: nextState,
        $push: {
          logs: {
            message: `Status changed to "${nextState}"`,
            time: new Date().toUTCString(),
          },
        },
      };
      if (nextState === 'Arrived to delivery') {
        update.status = 'SHIPPED';
        await updateTruckStatus(user, 'IS');
      }
      const success = await updateLoadStatusForDriver(user, update);
      if (!success) {
        return response.status(400).json({message: 'No active load'}).end();
      }
      response.status(200).json({
        message: `Load state changed to ${nextState}`,
      });
    } else {
      return response.status(400).json({message: 'Cannot update state'}).end();
    }
  } catch (error) {
    response.status(400).json({message: error.message}).end();
  };
};

module.exports.getLoads = getLoads;
module.exports.createLoad = createLoad;
module.exports.getActiveLoad = getActiveLoad;
module.exports.changeLoadState = changeLoadState;
