const {TruckModel} = require('../models/truck');
const {truckSchema, idSchema} = require('./../joiSchemas/schemas');

const isTruckIdProvided = (async (request, response, next) => {
  const {id} = request.params;
  const {error, value} = idSchema.validate(id);
  if (error) {
    return response.status(400).json({message: error.message});
  } else {
    request.params.id = value;
    next();
  }
});

const isTruckExists = (async (request, response, next) => {
  const {id} = request.params;
  try {
    const truck = await TruckModel.findById(id);
    if (!truck) {
      return response
          .status(400)
          .json({message: `Truck with id ${id} does not exists`})
          .end();
    }
    next();
  } catch (error) {
    return response
        .status(400)
        .json({message: `Truck with id ${id} does not exists`})
        .end();
  }
});

const isValidType = (async (request, response, next) => {
  const {error, value} = truckSchema.validate(request.body);
  if (error) {
    return response.status(400).json({message: error.message});
  } else {
    request.body = value;
    next();
  }
});

module.exports.isTruckExists = isTruckExists;
module.exports.isValidType = isValidType;
module.exports.isTruckIdProvided = isTruckIdProvided;
