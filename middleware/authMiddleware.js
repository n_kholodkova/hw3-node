const jwt = require('jsonwebtoken');
require('dotenv').config();
const {UserModel} = require('../models/user');

const {
  registrationCredentialsShema,
  credentialsShema,
  passwordSchema,
  emailSchema,
} = require('./../joiSchemas/schemas');

const authMiddleware = async (request, response, next) => {
  let JWTtoken =
    request.headers['authorization'] ??
    request.headers['Authorization'] ??
    null;
  if (!JWTtoken) {
    return response
        .status(400)
        .json({
          message: 'No access token.',
        })
        .end();
  }
  if (JWTtoken.split(' ').length === 2) {
    JWTtoken = JWTtoken.split(' ')[1];
  }
  jwt.verify(JWTtoken, process.env.JWT_SECRET, async function(error, decoded) {
    if (error) {
      return response
          .status(400)
          .json({message: 'Invalid authentication token.'});
    }
    request.userId = decoded.id;
    const user = await UserModel.findById(request.userId);
    if (!user) {
      request.userId = null;
      return response
          .status(400)
          .json({message: 'Invalid authentication token.'});
    }
    next();
  });
};

const vailidateOnSignUp = (request, response, next) => {
  const {error, value} = registrationCredentialsShema.validate(request.body);
  if (error) {
    return response.status(400).json({message: error.message});
  } else {
    request.body = value;
    next();
  }
};

const vailidateOnSignIn = (request, response, next) => {
  const {error, value} = credentialsShema.validate(request.body);
  if (error) {
    return response.status(400).json({message: error.message});
  } else {
    request.body = value;
    next();
  }
};

const vailidateNewPassword = (request, response, next) => {
  const {error, value} = passwordSchema.validate(request.body.newPassword);
  if (error) {
    return response.status(400).json({message: error.message});
  } else {
    request.body.newPassword = value;
    next();
  }
};

const vailidateEmail = (request, response, next) => {
  const {error, value} = emailSchema.validate(request.body.email);
  if (error) {
    return response.status(400).json({message: error.message});
  } else {
    request.body.email = value;
    next();
  }
};

module.exports.authMiddleware = authMiddleware;
module.exports.vailidateOnSignIn = vailidateOnSignIn;
module.exports.vailidateOnSignUp = vailidateOnSignUp;
module.exports.vailidateNewPassword = vailidateNewPassword;
module.exports.vailidateEmail = vailidateEmail;
