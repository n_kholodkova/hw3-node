const {loadSchema, idSchema} = require('../joiSchemas/schemas');
const {LoadModel} = require('../models/load');

const getLoadValid = (async (request, response, next) => {
  const {error, value} = loadSchema.validate(request.body);
  if (error) {
    return response.status(400).json({message: error.message});
  } else {
    request.body = value;
    next();
  }
});

const isLoadIdProvided = (async (request, response, next) => {
  const {id} = request.params;
  const {error, value} = idSchema.validate(id);
  if (error) {
    return response.status(400).json({message: error.message});
  } else {
    request.params.id = value;
    next();
  }
});

const isLoadExists = (async (request, response, next) => {
  const {id} = request.params;
  try {
    const load = await LoadModel.findById(id);
    if (!load) {
      return response
          .status(400)
          .json({message: `Load with id ${id} does not exists`})
          .end();
    }
    next();
  } catch (error) {
    return response
        .status(400)
        .json({message: `Load with id ${id} does not exists`})
        .end();
  }
});

const canLaodBeUpdated = (async (request, response, next) => {
  const {id} = request.params;
  try {
    const load = await LoadModel.findById(id);
    if (load.status !== 'NEW') {
      return response
          .status(400)
          .json({message: `Load with id ${id} cannot be updated or deleted`})
          .end();
    }
    next();
  } catch (error) {
    return response
        .status(400)
        .json({message: `Load with id ${id} does not exists`})
        .end();
  }
});

module.exports.getLoadValid = getLoadValid;
module.exports.isLoadIdProvided = isLoadIdProvided;
module.exports.isLoadExists = isLoadExists;
module.exports.canLaodBeUpdated = canLaodBeUpdated;
