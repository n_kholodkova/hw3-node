const {UserModel} = require('../models/user');

const isDriver = (async (request, response, next) => {
  const userId = request.userId;
  const user = await UserModel.findById(userId);
  if (user.role !== 'DRIVER') {
    return response
        .status(400)
        .json({message: `Denied. Access only for drivers`})
        .end();
  }
  next();
});

const isShipper = (async (request, response, next) => {
  const userId = request.userId;
  const user = await UserModel.findById(userId);
  if (user.role !== 'SHIPPER') {
    return response
        .status(400)
        .json({message: `Denied. Access only for shippers`})
        .end();
  }
  next();
});

module.exports.isDriver = isDriver;
module.exports.isShipper = isShipper;
