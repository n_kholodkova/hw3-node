const express = require('express');
const router = new express.Router();
const {getMe, updateMe, deleteMe} = require('./../controllers/me');
const {
  authMiddleware,
  vailidateNewPassword,
} = require('../middleware/authMiddleware');

router.get('/api/users/me', authMiddleware, (request, response) => {
  try {
    getMe(request, response);
  } catch (error) {
    response.status(500).json({error: 'Internal server error'});
  }
});

router.patch('/api/users/me/password',
    authMiddleware,
    vailidateNewPassword, (request, response) => {
      try {
        updateMe(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.delete('/api/users/me', authMiddleware, (request, response) => {
  try {
    deleteMe(request, response);
  } catch (error) {
    response.status(500).json({error: 'Internal server error'});
  }
});

module.exports.meRouter = router;
