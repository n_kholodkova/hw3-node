const express = require('express');
const router = new express.Router();
const {signUp, signIn, updatePassword} = require('./../controllers/auth');
const {
  vailidateOnSignIn,
  vailidateOnSignUp,
  vailidateEmail,
} = require('./../middleware/authMiddleware');

router.post('/api/auth/register', vailidateOnSignUp, (request, response) => {
  try {
    signUp(request, response);
  } catch (error) {
    response.status(500).json({error: 'Internal server error'});
  }
});

router.post('/api/auth/login', vailidateOnSignIn, (request, response) => {
  try {
    signIn(request, response);
  } catch (error) {
    response.status(500).json({error: 'Internal server error'});
  }
});

router.post('/api/auth/forgot_password',
    vailidateEmail,
    (request, response) => {
      try {
        updatePassword(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

module.exports.authRouter = router;
