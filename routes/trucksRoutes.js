const express = require('express');
const router = new express.Router();
const {
  getTrucks,
  createTruck,
  getTruck,
  updateTruck,
  assignTruck,
  deleteTruck,
} = require('./../controllers/trucks');
const {authMiddleware} = require('../middleware/authMiddleware');
const {
  isTruckExists,
  isValidType,
  isTruckIdProvided} = require('../middleware/trucksMiddleware');
const {
  isDriver,
} = require('../middleware/userMiddleware');

router.get('/api/trucks',
    authMiddleware,
    isDriver,
    (request, response) => {
      try {
        getTrucks(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.post('/api/trucks',
    authMiddleware,
    isDriver,
    isValidType,
    (request, response) => {
      try {
        createTruck(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.get('/api/trucks/:id',
    isTruckIdProvided,
    authMiddleware,
    isTruckExists,
    isDriver,
    (request, response) => {
      try {
        getTruck(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.put('/api/trucks/:id',
    isTruckIdProvided,
    authMiddleware,
    isTruckExists,
    isDriver,
    isValidType,
    (request, response) => {
      try {
        updateTruck(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.post('/api/trucks/:id/assign',
    isTruckIdProvided,
    authMiddleware,
    isTruckExists,
    isDriver,
    (request, response) => {
      try {
        assignTruck(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.delete('/api/trucks/:id',
    isTruckIdProvided,
    authMiddleware,
    isTruckExists,
    isDriver,
    (request, response) => {
      try {
        deleteTruck(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

module.exports.trucksRouter = router;
