const express = require('express');
const router = new express.Router();
const {
  getLoads,
  createLoad,
  getActiveLoad,
  changeLoadState,
} = require('../controllers/loads');
const {
  getLoad,
  updateLoad,
  postLoad,
  deleteLoad,
  getShippingInfo,
} = require('../controllers/loadsWithId');
const {authMiddleware} = require('../middleware/authMiddleware');
const {
  isLoadExists,
  canLaodBeUpdated,
  isLoadIdProvided,
} = require('../middleware/loadsMiddleware');
const {
  isDriver,
  isShipper,
} = require('../middleware/userMiddleware');
const {getLoadValid} = require('../middleware/loadsMiddleware');

router.get('/api/loads',
    authMiddleware,
    (request, response) => {
      try {
        getLoads(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.post('/api/loads',
    authMiddleware,
    isShipper,
    getLoadValid,
    (request, response) => {
      try {
        createLoad(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.get('/api/loads/active',
    authMiddleware,
    isDriver,
    (request, response) => {
      try {
        getActiveLoad(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.patch('/api/loads/active/state',
    authMiddleware,
    isDriver,
    (request, response) => {
      try {
        changeLoadState(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.get('/api/loads/:id',
    isLoadIdProvided,
    authMiddleware,
    isLoadExists,
    (request, response) => {
      try {
        getLoad(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.put('/api/loads/:id',
    isLoadIdProvided,
    authMiddleware,
    isLoadExists,
    canLaodBeUpdated,
    isShipper,
    getLoadValid,
    (request, response) => {
      try {
        updateLoad(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.delete('/api/loads/:id',
    isLoadIdProvided,
    authMiddleware,
    isLoadExists,
    canLaodBeUpdated,
    isShipper,
    (request, response) => {
      try {
        deleteLoad(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.post('/api/loads/:id/post',
    isLoadIdProvided,
    authMiddleware,
    isShipper,
    isLoadExists,
    canLaodBeUpdated,
    (request, response) => {
      try {
        postLoad(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

router.get('/api/loads/:id/shipping_info',
    isLoadIdProvided,
    authMiddleware,
    isShipper,
    isLoadExists,
    (request, response) => {
      try {
        getShippingInfo(request, response);
      } catch (error) {
        response.status(500).json({error: 'Internal server error'});
      }
    });

module.exports.loadsRouter = router;
