const Joi = require('joi');

const email = Joi.string().email().lowercase().required();
const password = Joi.string().min(5).required();
const id = Joi.string().trim().regex(/^[a-z0-9]+$/).required();
const loadStatus = Joi.string().trim()
    .valid('NEW', 'POSTED', 'ASSIGNED', 'SHIPPED').uppercase();
const loadLimit = Joi.number().positive().less(51);
const loadOffset = Joi.number().positive();

const dimentiosSchema = Joi.object({
  width: Joi.number().positive().greater(0).required(),
  length: Joi.number().positive().greater(0).required(),
  height: Joi.number().positive().greater(0).required(),
});

const credentialsShema = Joi.object({
  email: email,
  password: password,
});

const registrationCredentialsShema = Joi.object({
  email: email,
  password: password,
  role: Joi.string().valid('SHIPPER', 'DRIVER').uppercase().required(),
});

const truckSchema = Joi.object({
  type: Joi.string()
      .valid('SPRINTER', 'SMALL STRAIGHT', 'LARGE STRAIGHT')
      .uppercase()
      .required(),
});

const loadSchema = Joi.object({
  payload: Joi.number().positive().greater(0).required(),
  pickup_address: Joi.string().required(),
  delivery_address: Joi.string().required(),
  dimensions: dimentiosSchema,
  name: Joi.string().required(),
});

module.exports.credentialsShema = credentialsShema;
module.exports.registrationCredentialsShema = registrationCredentialsShema;
module.exports.truckSchema = truckSchema;
module.exports.loadSchema = loadSchema;
module.exports.passwordSchema = password;
module.exports.emailSchema = email;
module.exports.idSchema = id;
module.exports.loadStatusSchema = loadStatus;
module.exports.loadLimitSchema = loadLimit;
module.exports.loadOffsetSchema = loadOffset;

